from collections import defaultdict
import os

import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from vis.spanworld import SpanWorld
from vis.spanrectangle import SpanRectangle
from vis.spancircle import SpanCircle
from vis.spanline import SpanLine
from vis.spanpoly import SpanPoly

import pygimli.meshtools as mt


class PolygonHandler():
    """
    Handles triggering the differnet tools and takes care that the object
    parameters are sent to the correct location.
    """

    def __init__(self, parent=None):
        """

        Parameters
        ----------
        parent: GIModToolBar
        """
        self.parent = parent
        self.all_polys = {}
    
    def setPlotWidget(self, widget):
        self.plotwidget = widget

    def triggerPolyWorld(self, checked):
        """
        Instanciate the MPL PolyWorld to draw the model tank.

        Parameters
        ----------
        checked: bool
            The value if the action of polyworld whether is checked or not.
        """
        if checked:
            self.active_poly_btn = 'world'

            if hasattr(self, 'active_polytool'):
                self.active_polytool.disconnect()
            
            self.active_polytool = SpanWorld(self)
            self.active_polytool.connect()
        else:
            self.active_polytool.disconnect()
    
    def triggerPolyRectangle(self, checked):
        """
        Instanciate the MPL PolyRectangle.

        Parameters
        ----------
        checked: bool
            The value if the action of rectangle whether is checked or not.
        """
        if checked:
            self.active_poly_btn = 'rectangle'

            if hasattr(self, 'active_polytool'):
                self.active_polytool.disconnect()
            
            self.active_polytool = SpanRectangle(self)
            self.active_polytool.connect()
        else:
            self.active_polytool.disconnect()
    
    def triggerPolyCircle(self, checked):
        """
        Instanciate the MPL PolyCircle.

        Parameters
        ----------
        checked: bool
            The value if the action of circle whether is checked or not.
        """
        if checked:
            self.active_poly_btn = 'circle'

            if hasattr(self, 'active_polytool'):
                self.active_polytool.disconnect()
            
            self.active_polytool = SpanCircle(self)
            self.active_polytool.connect()
        else:
            self.active_polytool.disconnect()
    
    def triggerPolyLine(self, checked):
        """
        Instanciate the MPL PolyLine.

        Parameters
        ----------
        checked: bool
            The value if the action of line whether is checked or not.
        """
        if checked:
            self.active_poly_btn = 'line'

            if hasattr(self, 'active_polytool'):
                self.active_polytool.disconnect()
            
            self.active_polytool = SpanLine(self)
            self.active_polytool.connect()
        else:
            self.active_polytool.disconnect()
    
    def triggerPolygon(self, checked):
        """
        Instanciate the MPL PolyLine.

        Parameters
        ----------
        checked: bool
            The value if the action of line whether is checked or not.
        """
        if checked:
            self.active_poly_btn = 'polygon'

            if hasattr(self, 'active_polytool'):
                self.active_polytool.disconnect()
            
            self.active_polytool = SpanPoly(self)
            self.active_polytool.connect()
        else:
            self.active_polytool.disconnect()
    
    def setPolyAlpha(self, alpha):
        """The opacity value for the drawn polygons."""
        self._alpha = alpha
        if hasattr(self, 'active_polytool'):
            [
                p.set_alpha(alpha) for p in self.active_polytool.figure.ax.patches
            ]
    
    def drawPoly(self, kind, poly, coords):
        # print(kind, poly, coords)
        if kind == 'World' and 'World' not in self.all_polys.keys():
            self.all_polys['World'] = {
                'poly': poly,
                'data': coords
                }
            self.triggerPolyWorld(False)
            self.parent.acn_polyworld.setChecked(False)
            self.parent.acn_polyworld.setEnabled(False)
        else:
            num = len([n for n in self.all_polys.keys() if kind in n])
            self.all_polys[kind + '_' + str(num)] = {
                'poly': poly,
                'data': coords
            }

        print(self.all_polys)
        patches = [v['poly'] for v in self.all_polys.values()]
        self.active_polytool.drawToCanvas(patches)
    
    def createPLC(self):
        """pyGIMLi PLC."""
        polys = []
        for kind, v in self.all_polys.items():
            kind = kind.split('_')[0]
            print("*"*5, kind)
            plc = getattr(mt, 'create' + kind)
            if kind in ['World', 'Rectangle', 'Line']:
                polys.append(plc(
                    start=[v['data'][0], v['data'][1]],
                    end=[v['data'][2], v['data'][3]]
                ))
            elif kind == 'Circle':
                polys.append(plc(
                    pos=[v['data'][0], v['data'][1]],
                    radius=v['data'][2]
                ))
            elif kind == 'Polygon':
                polys.append(v['data'], closed=True)
        
        return mt.mergeMeshes(polys)


if __name__ == '__main__':
    pass
