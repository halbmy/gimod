#!/usr/bin/env python
# encoding: UTF-8

import os

from PyQt5.QtWidgets import (
    QMainWindow, QApplication, QStatusBar, QSplitter, QLabel
    )
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon

import sys
# import pygimli as pg
# from pygimli.meshtools import createMesh, exportPLC, exportFenicsHDF5Mesh, readPLC
# from pygimli.mplviewer import drawMeshBoundaries, drawMesh, drawPLC, drawModel
from widgets.toolbar import GIModToolBar
from widgets.tabholder import TabHolder
from vis.plotwindow import PlotWindow


class GIMod(QMainWindow):

    def __init__(self, parent=None):
        """.
        """
        super(GIMod, self).__init__(parent)
        self.setupWidget()

        # connect
        # self.toolbar.cbbx_accuracy.currentIndexChanged.connect(self.plotwindow.updateGrid)

    def setupWidget(self):
        """Set the GUI together from the other widgets."""
        # get the icon path
        self.ipath = os.path.join(os.path.dirname(__file__), 'icons')
        # instanciate the status bar to prompt some information of what is
        # going on beneath the hood
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)

        # instanciate the toolbar with the polytool functionality
        self.toolbar = GIModToolBar(self)
        self.addToolBar(self.toolbar)
        
        self.plotwindow = PlotWindow(self)

        self.tabholder = TabHolder(self)

        self.toolbar.ph.setPlotWidget(self.plotwindow)
        # tile the GUI in two resizable sides
        splitter = QSplitter(Qt.Horizontal)
        splitter.addWidget(self.tabholder)
        splitter.addWidget(self.plotwindow)

        self.setCentralWidget(splitter)

        self.statusbarWidgets()

        # self.setGeometry(1500, 100, 1000, 600)
        # window name
        self.setWindowTitle("GIMod")
        self.setWindowIcon(QIcon('icons/logo.png'))
        self.show()
    
    def statusbarWidgets(self):
        self.w_xyposition = QLabel()

        self.statusbar.addWidget(self.w_xyposition)


if __name__ == '__main__':

    app = QApplication(sys.argv)
    app.setApplicationName("GIMod")

    main = GIMod()
    main.show()

    sys.exit(app.exec_())
