import sys

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.pyplot import tight_layout

import numpy as np
import random

from scipy.misc import imread
from PyQt5.QtWidgets import QPushButton, QVBoxLayout, QWidget


class PlotWindow(QWidget):
    def __init__(self, parent=None):
        super(PlotWindow, self).__init__(parent)
        self.parent = parent
        # a figure instance to plot on
        self.figure = Figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.hide()

        # set the layout
        layout = QVBoxLayout()
        layout.addWidget(self.canvas)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
        self.getActions()
        self.plot()
        # self.updateGrid()

        # self.cid_p = self.canvas.mpl_connect('motion_notify_event', self.onMouseMove)
    
    def getActions(self):
        for acn in self.toolbar.actions():
            if not acn.isSeparator():
                self.parent.toolbar.addToToolBar(acn)

    def plot(self):
        ''' plot some random stuff '''
        # random data
        # data = [random.random() for i in range(10)]

        # create an axis
        self.ax = self.figure.add_subplot(111)
        self.ax.set_aspect('equal')
        self.ax.set_xlim([-5, 5])
        self.ax.set_ylim([-5, 0])
        self.ax.grid(alpha=0.5, lw=0.5)

        # discards the old graph
        # self.ax.clear()

        # plot data
        # self.ax.plot(data, '*-')

        # refresh canvas
        self.canvas.draw()
    
    # def updatePrecision(self):
    #     """
    #     Calculate click precision for drawing Polygons.

    #     The QComboBox from Toolbar is taken the positions around the comma is counted. From that
    #     the precision is derived.
    #     """
    #     _precision = self.parent.toolbar.cbbx_accuracy.currentText()
    #     pts = _precision.split('.')
    #     if len(pts) == 2:
    #         pts = pts[1]
    #         self.precision = len(pts)

    #     else:
    #         self.precision = -(len(*pts) - 1)

    # def updateGrid(self):
    #     print("UPDATE GRID")
    #     # self.updatePrecision()
    #     _x = self.ax.get_xticks()
    #     _y = self.ax.get_yticks()

    #     x, y = np.meshgrid(_x, _y)

    #     # delete old points
    #     if hasattr(self, 'sc'):
    #         self.sc.remove()
    #     self.sc = self.ax.scatter(x, y, c='#aaaaaa', marker='+', lw=0.5)

    # def onMouseMove(self, event):
    #     """.

    #     Parameters
    #     ----------
    #     event: MPL MouseEvent
    #     """
    #     if event.inaxes:

    #         self.curr_x = round(event.xdata, self.precision)
    #         self.curr_y = round(event.ydata, self.precision)
    #         # print(event.xdata, event.ydata)
    #         # print(self.curr_x, self.curr_y)

    #         if hasattr(self, 'cp'):
    #             self.cp.remove()
    #         self.cp = self.ax.scatter(self.curr_x, self.curr_y, c='#ff0000', marker='+')
    #         self.canvas.draw_idle()

    def setBackgroundImage(self, fpath):
        """
        .
        
        Parameters
        ----------
        fpath: str
            Path to the image that will be loaded as background
        """
        self.global_alpha = 0.5
        img = imread(fpath, flatten=True)
        _y, _x = img.shape
        self.bimg = self.ax.imshow(np.flip(img), cmap='gray')
        self.ax.set_xlim([0, _x])
        self.ax.set_ylim([0, _y])
        self.canvas.draw()
    
    def removeBackground(self):
        self.bimg.remove()
        self.canvas.draw()


if __name__ == '__main__':
    pass
