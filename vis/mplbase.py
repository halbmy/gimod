"""
The matplotlib basiscs for the polygon behaviour.
"""

from matplotlib import pyplot as plt
from matplotlib import patches
from matplotlib import lines
import numpy as np

from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import Qt, pyqtSignal


class MPLBase(object):
    """Hold the general behaviour of the drawn polygons."""

    # finished = pyqtSignal(str, list)

    def __init__(self, parent=None):
        """."""
        super(MPLBase, self).__init__()
        self.parent = parent  # PolygonHandler
        self.figure = parent.plotwidget

    def connect(self):
        """."""
        print("CONNECT")
        self.cid_p = self.figure.canvas.mpl_connect(
            'button_press_event', self.onPress)
        self.cid_dp = self.figure.canvas.mpl_connect(
            'button_press_event', self.onPress)
        self.cid_mm = self.figure.canvas.mpl_connect(
            'motion_notify_event', self.onMotion)
        self.cid_mg = self.figure.canvas.mpl_connect(
            'motion_notify_event', self._magnetGrid)
        self.cid_r = self.figure.canvas.mpl_connect(
            'button_release_event', self.onRelease)
        self.cid_ae = self.figure.canvas.mpl_connect(
            'axes_enter_event', self.axesEnter)
        self.cid_al = self.figure.canvas.mpl_connect(
            'axes_leave_event', self.axesLeave)

    def disconnect(self, cid=None):
        """Disconnect all the stored connection ids."""
        print("DISCONNECT")
        if cid is not None:
            self.figure.canvas.mpl_disconnect(getattr(self, cid))
        else:
            for cid in ['cid_p', 'cid_dp', 'cid_mm', 'cid_r', 'cid_ae', 'cid_al']:
                if hasattr(self, cid):
                    self.disconnect(cid)
        
        if hasattr(self, 'cp'):
            try:
                self.cp.remove()
            except ValueError:
                pass
    
    def axesLeave(self, event):
        """Allow the standard cursors outside the drawing area."""
        QApplication.restoreOverrideCursor()

    def axesEnter(self, event):
        """Prepend matplotlibs new feature to create a QWaitCursor while dragging some stuff."""
        QApplication.setOverrideCursor(QCursor(Qt.ArrowCursor))

    def drawToCanvas(self, patches):
        """Convenience function to add the gathered path to the current figure."""
        # self.figure.ax.cla()
        # if self.parent.parent.toolbar.acn_imageAsBackground.isChecked():
        #     alpha = 0.2
        #     self.parent.parent.image_tools.setBackground()
        # else:
        #     alpha = 1
        # [p.remove() for p in reversed(self.figure.ax.patches)]
        cmap = plt.cm.get_cmap('Set3', len(patches))
        alpha = self.parent._alpha if hasattr(self.parent, '_alpha') else 1
        for i, patch in enumerate(patches):
            # patch.set_fc((*cmap(i)[:3], 0.8))
            if isinstance(patch, lines.Line2D):
                patch.set_lw(2)
                patch.set_color((*cmap(i)[:-1], alpha))
                self.figure.ax.add_line(patch)
            else:
                patch.set_lw(1)
                patch.set_fc((*cmap(i)[:-1], alpha))
                patch.set_ec('#000000')
                self.figure.ax.add_patch(patch)

        # verts = self.parent.magnets[0]
        # x = [v[0] for v in verts]
        # y = [v[1] for v in verts]
        # self.figure.ax.set_xlim([min(x), max(x)])
        # self.figure.ax.set_ylim([min(y), max(y)])
        # self.figure.ax.grid(lw=0.5, alpha=0.5)
        # redraw the magnet positions after adding a new patch
        # if self.parent.parent.toolbar.acn_magnetizePoly.isChecked():
        #     self.parent.mp.plotMagnets()

        # self.figure.canvas.draw()
        # self.figure.canvas.draw_idle()
        # self.figure.canvas.flush_events()

    def _magnetGrid(self, event):
        """.

        Parameters
        ----------
        event: MPL MouseEvent
        """
        if event.inaxes:
            # print("magnet move")
            self.curr_x = round(event.xdata, self.parent.parent.precision)
            self.curr_y = round(event.ydata, self.parent.parent.precision)
            # print(event.xdata, event.ydata)
            # print(self.curr_x, self.curr_y)

            # if hasattr(self, 'cp'):
            try:
                self.cp.remove()
            except (ValueError, AttributeError):
                # unknown reasons
                pass
            self.parent.parent.parent.w_xyposition.setText(
                "x: {}, y: {}".format(self.curr_x, self.curr_y)
                )
            self.cp = self.figure.ax.scatter(
                self.curr_x, self.curr_y, c='#ff0000', marker='+', zorder=100)
            # self.figure.canvas.draw_idle()
            self.figure.canvas.draw()
