from PyQt5.QtWidgets import (
    QToolBar, QAction, QGroupBox, QWidget, QHBoxLayout, QComboBox
)
from PyQt5.QtGui import QIcon

import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from core.polygonhandler import PolygonHandler# as ph


class GIModToolBar(QToolBar):

    def __init__(self, parent=None):
        """.
        
        Parameters
        ----------
        parent: GIMod
        """
        super(GIModToolBar, self).__init__(parent)
        self.parent = parent
        self.setupWidget()

        self.cbbx_accuracy.currentIndexChanged.connect(self.updatePrecision)

        self.ph = PolygonHandler(self)
        self.acn_polyworld.triggered.connect(self.ph.triggerPolyWorld)
        self.acn_polyrect.triggered.connect(self.ph.triggerPolyRectangle)
        self.acn_polycirc.triggered.connect(self.ph.triggerPolyCircle)
        self.acn_polyline.triggered.connect(self.ph.triggerPolyLine)
        self.acn_polygon.triggered.connect(self.ph.triggerPolygon)

    def setupWidget(self):
        """Set Layout."""
        self.ipath = self.parent.ipath + '/'
        self.acn_grid = QAction(QIcon(self.ipath + 'grid.svg'), "net", checkable=True)
        self.acn_grid.setChecked(True)
        self.cbbx_accuracy = QComboBox()
        self.cbbx_accuracy.addItems(
            ["100", "10", "1", "0.1", "0.01", "0.001", "0.0001"]
            )
        self.cbbx_accuracy.setCurrentIndex(3)

        self.addAction(self.acn_grid)
        # self.addAction(self.acn_gridx)
        # self.addAction(self.acn_gridy)
        self.addWidget(self.cbbx_accuracy)
        self.addSeparator()
        self._createPolygonActions()
        self.addHStretch()
        # here the MPL toolbar is added

        self.updatePrecision()

    def addHStretch(self):
        w = QWidget()
        self.h = QHBoxLayout()
        self.h.addStretch(1)
        w.setLayout(self.h)
        self.addWidget(w)
    
    def _createPolygonActions(self):
        """Subbed to keep overview as light as possible."""
        self.acn_polyworld = QAction(
            QIcon(self.ipath + 'poly_world.svg'), 'PolyWorld',
            checkable=True
            )
        self.acn_polyrect = QAction(
            QIcon(self.ipath + 'poly_rectangle.svg'), 'PolyRectangle', 
            checkable=True
            )
        self.acn_polycirc = QAction(
            QIcon(self.ipath + 'poly_circle.svg'), 'PolyCircle', 
            checkable=True
            )
        self.acn_polyline = QAction(
            QIcon(self.ipath + 'poly_line.svg'), 'PolyLine', 
            checkable=True
            )
        self.acn_polygon = QAction(
            QIcon(self.ipath + 'poly_gon.svg'), 'Polygon', 
            checkable=True
            )
        self.addAction(self.acn_polyworld)
        self.addAction(self.acn_polyrect)
        self.addAction(self.acn_polycirc)
        self.addAction(self.acn_polyline)
        self.addAction(self.acn_polygon)

    def addToToolBar(self, acn):
        """
        .
        
        Parameters
        ----------
        acn: QAction
            Action from matplotlibs NavigationToolBar.
        
        Note
        ----
        Called from vis.plotwindow.py
        """
        if acn.text() != '':
            self.addAction(acn)
    
    def updatePrecision(self):
        """
        Calculate click precision for drawing Polygons.

        The QComboBox from Toolbar is taken the positions around the comma is counted. From that
        the precision is derived.
        """
        _precision = self.cbbx_accuracy.currentText()
        pts = _precision.split('.')
        if len(pts) == 2:
            pts = pts[1]
            self.precision = len(pts)

        else:
            self.precision = -(len(*pts) - 1)


if __name__ =='__main__':
    pass
