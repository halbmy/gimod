from PyQt5.QtWidgets import QTabWidget

from .tabs.tabimage import ImageTab


class TabHolder(QTabWidget):

    def __init__(self, parent=None):
        super(TabHolder, self).__init__(parent)
        self.parent = parent  # GIMod
        self.setup()
    
    def setup(self):
        """Design the Layout."""
        self.setTabPosition(QTabWidget.West)
        self.imagetab = ImageTab(self)
        self.addTab(self.imagetab, 'Image Scan')
