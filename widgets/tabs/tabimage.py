from pygimli.meshtools import exportPLC

from PyQt5.QtWidgets import (
    QWidget, QLineEdit, QPushButton, QHBoxLayout, QVBoxLayout, QFileDialog
)
# from PyQt5.QtCore import QFileSelector


class ImageTab(QWidget):

    def __init__(self, parent=None):
        super(ImageTab, self).__init__(parent)
        self.parent = parent  # TabHolder
        self.setup()

        # connect btn_browse signal
        self.btn_browse.clicked.connect(self.browseForImage)
        self.btn_setbackground.clicked.connect(self.prepareBackgroundImage)
        self.btn_savePLC.clicked.connect(self.createAndSavePLC)
    
    def setup(self):
        """Design the Layout."""
        self.le_filepath = QLineEdit(
            "/home/frodo/Downloads/tannwald-linesSWclean.png")
        # self.le_filepath.setPlaceholderText("/path/to/image")

        self.btn_browse = QPushButton("Browse")
        
        self.btn_setbackground = QPushButton("Set As Background")
        self.btn_setbackground.setCheckable(True)
        # self.btn_setbackground.setEnabled(False)

        self.btn_savePLC = QPushButton("Save PLC")

        # oragnize the two widgets for file search in a hbox
        fbox = QHBoxLayout()
        fbox.addWidget(self.le_filepath)
        fbox.addWidget(self.btn_browse)
        fbox.setContentsMargins(0, 0, 0, 0)

        cbox = QVBoxLayout()
        cbox.addLayout(fbox)
        cbox.addWidget(self.btn_setbackground)
        cbox.addStretch(1)
        cbox.addWidget(self.btn_savePLC)
        cbox.setContentsMargins(0, 0, 0, 0)

        self.setLayout(cbox)
    
    def browseForImage(self):
        """When clicked open a standard dialog to select an image file."""
        fname = QFileDialog.getOpenFileName()
        # print(fname)
        if fname[0] != '':
            self.le_filepath.setText(fname[0])
            self.btn_setbackground.setEnabled(True)
    
    def prepareBackgroundImage(self, checked):
        """
        .
        
        Parameters
        ----------
        checked: bool
            Whether or not the background button is pressed.
            This decides whether the image is plotted or deleted.
        """
        gimod = self.parent.parent
        if checked:
            gimod.plotwindow.setBackgroundImage(self.le_filepath.text())
            gimod.toolbar.ph.setPolyAlpha(0.5)
        else:
            gimod.plotwindow.removeBackground()
            gimod.toolbar.ph.setPolyAlpha(1)
    
    def createAndSavePLC(self):
        gimod = self.parent.parent
        plc = gimod.toolbar.ph.createPLC()
        fname = QFileDialog.getSaveFileName(caption='Save as .plc')
        if fname[0] is not '':
            if not fname[0].endswith('.plc'):
                fname[0] += '.plc'
            exportPLC(plc, fname[0])


if __name__ == '__main__':
    pass
